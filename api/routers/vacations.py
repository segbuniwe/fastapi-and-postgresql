from fastapi import APIRouter, Depends
from queries.vacations import VacationIn, VactionRepository

router = APIRouter()


@router.post("/vacations")
def create_vacations(
    vacation: VacationIn,
    repo: VactionRepository = Depends()
):
    return repo.create(vacation)
