from queries.pool import pool
from pydantic import BaseModel
from typing import Optional
from datetime import date


# class Thought(BaseModel):
#     private_thoughts: str
#     public_thoughts: str


class VacationIn(BaseModel):
    name: str
    from_date: date
    to_date: date
    thoughts: Optional[str]


class VacationOut(BaseModel):
    id: int
    name: str
    from_date: date
    to_date: date
    thoughts: Optional[str]


class VactionRepository:
    def create(self, vacation: VacationIn) -> VacationOut:
        # connect to database
        with pool.connection() as conn:
            # get a cursor 9something to run SQL with
            with conn.cursor() as db:
                # run our INSERT statement
                result = db.execute(
                    """
                    INSERT INTO vacations
                        (name, from_date, to_date, thoughts)
                    VALUES
                        (%s, %s, %s, %s)
                    RETURNING id;
                    """,
                    [
                        vacation.name,
                        vacation.from_date,
                        vacation.to_date,
                        vacation.thoughts
                    ]
                )
                id = result.fetchone()[0]
                # return new data
                old_data = vacation.dict()
                return VacationOut(id=id, **old_data)
